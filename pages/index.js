import Head from 'next/head'
import Footer from '../components/Footer'
import Navbar from '../components/Navbar'
import Link from 'next/link'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <div>
      <h1 className={styles.title}>Homepage</h1>
      <p className={styles.text}>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Aliquam ipsa laboriosam laudantium, nulla molestiae tempora eos recusandae enim perferendis quo adipisci dicta provident repudiandae ratione corporis unde ea nisi sit?</p>
      <p className={styles.text}>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Aliquam ipsa laboriosam laudantium, nulla molestiae tempora eos recusandae enim perferendis quo adipisci dicta provident repudiandae ratione corporis unde ea nisi sit?</p>
      <Link href="/ninjas">
        <a className={styles.btn}>See Ninja Listing</a>
      </Link>
    </div>
  )
}
